import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton ramenButton;
    private JButton ebichiriButton;
    private JButton yurinchiButton;
    private JTextPane OrderedItemList;
    private JButton chahanButton;
    private JButton chinjaorosuButton;
    private JButton gomadangoButton;
    private JButton checkOutButton;
    private JLabel TotalPrice;
    private JLabel TotalLabel;

    int total;

    void order(String food,int Price,int PlusPrice){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "? it's " + Price + "yen",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            String currentText = OrderedItemList.getText();
            OrderedItemList.setText(currentText + food + " " + Price + "yen" + "\n");
            total += Price;

            int confirmation2 = JOptionPane.showConfirmDialog(null,
                    "Would you like to make a bigger? (+" + PlusPrice + "yen)",
                    "Size Confirmation",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation2 == 0) {
                currentText = OrderedItemList.getText();
                OrderedItemList.setText(currentText + "big size (+" + PlusPrice + "yen)" + "\n");
                total += PlusPrice;
                JOptionPane.showMessageDialog(null,"Thank you for ordering big " + food +
                        "! It will be served as soon as possible!");
            }
            else {
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food +
                        "! It will be served as soon as possible!");
            }
            TotalPrice.setText(total + " yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodGUI() {
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",400,100);
            }
        });
        ramenButton.setIcon(new ImageIcon( this.getClass().getResource("picture/ラーメン.jpg")));
        ebichiriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shrimp chili",540,120);
            }
        });
        ebichiriButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/エビチリ.jpg")));
        yurinchiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yurinchi",500,150);
            }
        });
        yurinchiButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/油淋鶏.jpg")));
        chahanButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chahan",300,100);
            }
        });
        chahanButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/炒飯.jpeg")));
        chinjaorosuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Chinjaolose",420,80);
            }
        });
        chinjaorosuButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/青椒肉絲.png")));
        gomadangoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sesame ball",150,70);
            }
        });
        gomadangoButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/胡麻団子.jpeg")));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to Checkout? ",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    OrderedItemList.setText(null);
                    TotalPrice.setText(null);
                    if (total < 3000) {
                        JOptionPane.showMessageDialog(null,
                                "Thank you! The total price is " + total + "yen!");
                    }
                    else{
                        JOptionPane.showMessageDialog(null,
                                "Thank you! The total price is " + total + "yen!\n" +
                                        "You can receive a discount ticket!");
                    }
                    total = 0;
                    TotalPrice.setText(total + " yen");
                }
            }
        });
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
